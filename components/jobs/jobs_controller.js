var app = angular.module('firstjob');

app.controller('JobCtrl', function(FJAPI, $state, JobService){
	var vm = this;
	vm.jobs = [];
	vm.jobsLoaded = false;
	vm.currentPage = 1;
	vm.pageSize = 30;
	vm.totalJobs = 30;
	vm.counters = {};
	vm.countersAll = "...";
	vm.currentState = $state.current.name;
	vm.showFilter = false;
	vm.searchValue = '';
	vm.source = "all-jobs"
	vm.dataFilters = {};

	loadJobs();
	getDataForFilters();

	vm.clickJobCard = function(){
		vm.showFilter = false;
	}

	vm.toggleFilter = function(){
		vm.showFilter = !vm.showFilter;
	}

	vm.jobFilter = function(){
		var careers_ids =  $("#career-filter").val();
		var experience = $("#jobs-filters-experience").val();
		var career_status = $("#jobs-filters-career-status").val();
		var looking_for = $("#jobs-filters-loooking-for").val();
		data = {ids: careers_ids, experience: experience, looking_for: looking_for, career_status: career_status};
		JobService.getFilteredJobs({job: data}, function(res){			
			vm.jobs =  res.data.jobs;
			vm.totalJobs = res.data.total;
			vm.currentPage = 1;
		}, function(err){console.log(err);});
	}

	vm.pageChanged = function(newPage) {
		vm.jobs = [];
		vm.totalJobs = 0;
		loadJobs();
	};

	function loadJobs(){
		switch($state.current.name){
			case "jobs":
			allJobs();
			break;
			case "job-favorites":
			favorites();
			break;
			case "job-applications":
			applications();
			break;
			case "job-removed":
			removed();
			break;
			default:
			allJobs();
		}
	}

	function getDataForFilters(){
		JobService.getDataForFilters(function(response){
			vm.dataFilters = response.data;
			$("#career-filter").select2({});
		}, function(err){
			console.log();
		});
	}

	function allJobs(){
		JobService.getJobs(vm.currentPage, function(res){
			vm.jobs =  res.data.jobs;
			vm.totalJobs = res.data.total;
			vm.jobsLoaded = true;
		}, function(err){
			vm.jobs = [];
			console.log(err);
		});
	}

	function favorites(){
		FJAPI.getFavorites(function(response){
			vm.jobs = response.data.jobs;
			vm.totalJobs = response.data.total;
			vm.jobsLoaded = true;
		}, function(err){ console.log(err); })
	}

	function applications(){
		FJAPI.getApplications(function(response){
			vm.jobs = response.data.jobs;
			vm.totalJobs = response.data.total;
			vm.jobsLoaded = true;
		}, function(err){
			console.log(err);
		});
	}

	function removed(){
		FJAPI.getRemoved(function(response){
			vm.jobs = response.data.jobs;
			vm.totalJobs = response.data.total;
			vm.jobsLoaded = true;
		}, function(err){
			console.log(err);
		});
	}

});

app.controller('JobShow', function(FJAPI, $stateParams, UserService, $state, $scope, ModalService, JobService, $rootScope){
	var vm = this;
	vm.questions = [];
	vm.answers = [];
	vm.job_id = $stateParams.job_id;
	vm.getJobQuestions = getJobQuestions;
	vm.sendAnswers = sendAnswers;
	vm.minimum_salary_expected = 0;
	vm.setJobSalary = setJobSalary;
	vm.applyJob = applyJob;
	vm.getJobApplication = getJobApplication;
	vm.removeApplication = removeApplication;
	vm.nextButtonText = nextButtonText;
	vm.job = null;
	vm.showShare = false;

	if (vm.job_id){ getJobInfo(); }

	vm.shareButtons = function(){
		vm.showShare = !vm.showShare;
	}

	vm.shareFacebook = function(){
		window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent($rootScope.currentURL()), 'facebook-share-dialog', 'width=626,height=436');
		return false;
	}

	vm.shareLinkedin = function(){
		window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + $rootScope.currentURL() + '&source=FirstJob', 'facebook-share-dialog', 'width=626,height=436');
		return false;
	}

	vm.editApplication = function(){
		UserService.setShowSalaryBackButton(0);
		ModalService.open({}, 'lg', 'components/jobs/job-salary.html');
	}

	function getJobInfo(){
		JobService.getJob(vm.job_id, function(res){
			vm.job =  res.data.job;
			vm.apply = res.data.apply != null ? true : false ;
			vm.company = res.data.company;
			vm.other_jobs_from_company = res.data.other_jobs_from_company;
			vm.similar_jobs = res.data.similar_jobs;
		}, function(err){
			vm.job = {};
		});
	}

	function getJobApplication(){
		FJAPI.getJobApplication(vm.job_id, function(response){
			if (response.data){
				vm.minimum_salary_expected = formatNumber(response.data.salary_expected);
			}
		}, function(err){
			console.log(err);
		});
	}

	function nextButtonText(){
		if (UserService.getResumeUrl() != null){ return "Finalizar postulación"; }
		return "Siguiente";
	}

	function applyJob(){
		if ( localStorage.getItem("firstjob_user") == null ){
			ModalService.open({}, 'lg', 'components/home/modals/modal-login.html');
		}
		else
		{
			if (UserService.getUserData().step == "finished"){
				UserService.setShowSalaryBackButton(0);
				ModalService.open({}, 'lg', 'components/jobs/job-salary.html');
			}
			else
			{
				UserService.setShowSalaryBackButton(1);
				ModalService.open({}, 'lg', 'components/users/new_user_data.html');
			}
		}
	}

	function getJobQuestions(){
		JobService.getJobQuestions(vm.job_id, function(response){ 
			vm.questions = response.data;

			FJAPI.getJobAnswers(vm.job_id, function(res){
				for (var i=0; i < response.data.length; i++){
					var value = res.data.length > 0 ? res.data[i].content : '';
					var questionData = {question_id: response.data[i].id, value: value };
					vm.answers.push( questionData );
				}
			}, function(err){ console.log(err);});

		}, function(err){ console.log(err); });
	}

	function sendAnswers(){
		if (vm.answers.length == 0){
			ModalService.dismissLastModal();
			ModalService.open({}, 'lg', 'components/jobs/job-apply-end.html');
		}

		FJAPI.answerJobQuestions(vm.job_id, vm.answers, function(response){
			ModalService.dismissLastModal();
			if (UserService.getUserData().resume == null){
				ModalService.open({}, 'lg', 'components/users/new_user_cv.html');
			}
			else
			{
				ModalService.open({}, 'lg', 'components/jobs/job-apply-end.html');
			}
		}, function(err){
			console.log(err);
		});
	}

	function setJobSalary(){
		var application = { job_id: vm.job_id, minimum_salary_expected: vm.minimum_salary_expected };

		if (to_s(application.minimum_salary_expected).indexOf(".") > 0){
			application.minimum_salary_expected = application.minimum_salary_expected.split('.').join("");
		}
		
		JobService.setJobSalary({application: application}, function(res){
			if (vm.job.total_question > 0){
				ModalService.open({}, 'lg', 'components/jobs/job-questions.html');
			}
			else
			{
				ModalService.open({}, 'lg', 'components/jobs/job-apply-end.html');
			}
		}, function(err){
			console.log(err);
		});
	}

	function removeApplication(job_id){
		FJAPI.removeJobApplication(job_id, function(response){
			ModalService.dismissLastModal();
			location.reload();
		}, function(err){
			console.log(err);
		});
	}

});