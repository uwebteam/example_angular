angular.module('firstjob')
.factory("JobService", function(UserService, HTML){
  var counters = {};
  var countersAll = {};

  var makeRequest = HTML.makeRequest;

  var getCounters = function(success){
    makeRequest(function(res){
      counters = res.data;
      countersAll = counters.all_jobs - counters.fav_jobs - counters.apply_jobs - counters.removed_jobs;
      success();
    }, function(err){ console.log(err); }, "GET", {}, "user/counter-jobs-user.json");
  }

  if (UserService.isUser() == true){
    getCounters(function(){});
  }

  var apply_jobs = function(){
    return counters.apply_jobs;
  }

  var countersAll = function(){
    return countersAll;
  }

  var fav_jobs = function(){
    return counters.fav_jobs;
  }

  var removed_jobs = function(){
    return counters.removed_jobs;
  }

  var getJobs = function(page, success, error){
    makeRequest(success, error, 'GET', {}, "jobs.json?page=" + page);
  };

  var getJob = function(id, success, error){
    makeRequest(success, error, 'GET', {}, "jobs/" + id + ".json");
  };

  var getJobQuestions = function(job_id, success, error){
    makeRequest(success, error, "GET", {}, "jobs/" + job_id + "/questions.json");
  }

  var setJobSalary = function(application, success, error){
    makeRequest(success, error, "POST", application, "job/apply.json");
  }

  var getDataForFilters = function(success, error){
    makeRequest(success, error, "GET", {}, "job/data-for-filters");
  }

  var getFilteredJobs = function(data, success, error){
    makeRequest(success, error, "POST", data, "job/get-filtered-jobs");
  }

  return {
    apply_jobs: apply_jobs,
    countersAll: countersAll,
    fav_jobs: fav_jobs,
    removed_jobs: removed_jobs,
    getCounters: getCounters,
    getJobs: getJobs,
    getJob: getJob,
    setJobSalary: setJobSalary,
    getDataForFilters: getDataForFilters,
    getJobQuestions: getJobQuestions,
    getFilteredJobs: getFilteredJobs
  }

});