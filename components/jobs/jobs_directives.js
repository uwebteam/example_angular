var app = angular.module('firstjob');

app.directive('favorite', function(FJAPI, JobService){
  return {
    restrict: 'A',
    scope: true,
    scope: {
      jobid: "=",
      favsource: "=",
      isfavorite: "="
    },

    template: '<i class="fa fa-heart fa-lg button-color" aria-hidden="true" ng-click="click()" data-fav="{{isfav}}" title="Marcar este anuncio como favorito."></i>',

    controller: function($scope, $element){
      $scope.isfav = $scope.isfavorite;
      $scope.click = function(){
        FJAPI.clickFavorite({job_id: $scope.jobid, source: $scope.favsource}, function(response){
          $scope.isfav = !response.data.favorite;
          $("[data-favorite-id=" + $scope.jobid+"]").attr("data-show", $scope.isfav);
          JobService.getCounters(function(){
          });
        }, function(err){
          console.log(err);
        });
      }
    }
  }
});

app.directive('remove', function(FJAPI, JobService){
  return {
    restrict: 'A',
    scope: true,
    scope: {
      jobid: "=",
      removesource: "=",
      isremoved: "="
    },

    template: '<i class="fa fa-trash fa-lg button-color" aria-hidden="true" ng-click="click()" data-removed="{{isrem}}" title="Remover este trabajo de la lista."></i>',

    controller: function($scope, $element){
      $scope.isrem = $scope.isremoved;
      $scope.click = function(){
        FJAPI.clickRemove({job_id: $scope.jobid, source: $scope.removesource}, function(response){
          $scope.isrem = !response.data.removed;
          $("[data-remove-id=" + $scope.jobid+"]").attr("data-show", $scope.isrem);
          JobService.getCounters(function(){
          });

        }, function(err){
          console.log(err);
        });
      };
    }
  }
});

app.directive('showmore', function(FJAPI, UserService){
  /*
    button-type=["show-more", "get-more"]
    source-show-more = ["all-jobs", "company", "similar"]
    source-get-more = ["company", "similar"]
    */
    return {
      restrict: 'A',
      scope: true,
      scope: {
        jobsource: "=",
        jobid: "=",
        buttontype: "=",
        source_utm: "="
      },
      link: function(scope, elem, attrs){
        elem.bind('click', function() {
        data = {button_type: scope.buttontype, job_id: scope.jobid, source: scope.jobsource, source_utm: ""}; //pendind source utm

        if (UserService.isUser() == false) {
          publicToken = UserService.getPublicToken();
          data["public_token"] = publicToken;
        }

        FJAPI.buttonClickTracking(data, function(response){
        }, function(err){
          console.log(err);
        });
      });
      }
    }
  });