var app = angular.module('firstjob');

app.controller('NewUserCtrl', function(FJAPI, $state, $stateParams, ModalService, $scope){
  var vm = this;
  var user = JSON.parse( localStorage.getItem("firstjob_user"));
  vm.user = {first_name: user.first_name, last_name: user.last_name, country_id: user.country_id, phone: user.phone, rut_dni: user.rut_dni };
  if (vm.user.first_name == "Usuario"){
    vm.user.first_name = '';
  }
  vm.countries = [];
  vm.submit_data = submit_data;
  vm.country_id = '';
  vm.job_id = $stateParams.job_id;
  vm.loading = true;
  vm.iso_code = '';
  vm.calling_code = '';
  $scope.country_select = '';

  $scope.$watch('country_select', function(newValue, oldValue) {
    change_country_phone(newValue);
  });

  FJAPI.getCountries(function(response){
    vm.countries = response.data;
    vm.country_id = to_s(vm.user.country_id);
    $scope.country_select = vm.country_id;
    vm.loading = false;
  }, function(err){ console.log(err); });

  function submit_data(){
    vm.user.country_id = parseInt(vm.country_id);
    var user = {users: vm.user};
    user.users.rut_dni = $("[data-type=rut]").val();

    if (!$.validateRut(user.users.rut_dni)){
      alert('Rut no valido');
      return;
    }

    FJAPI.newUserData(user, function(response){
      localStorage.setItem("firstjob_user", JSON.stringify(response.data));
      ModalService.open({}, 'lg', 'components/users/new_user_academic.html');
    }, function(err){ console.log(err); });
  }

  function change_country_phone(country_id){
    if (country_id == "" || country_id == "undefined" || country_id == null){
      return;
    }
    for (var i=0; i < vm.countries.length; i++){
      if (vm.countries[i].id == country_id){
        vm.iso_code = vm.countries[i].iso_code;
        vm.calling_code = vm.countries[i].calling_code;
        return;
      }
    }
  }

});

app.controller("NewUserAcademicCtrl", function(FJAPI, $scope, $state, $stateParams, ModalService){
  var vm = this;
  var user = JSON.parse( localStorage.getItem("firstjob_user"));
  vm.user = { university_id: user.university_id, principal_career_id: user.principal_career_id };
  vm.country_id = to_s(user.country_id);
  vm.university_id = to_s(user.university_id);
  vm.principal_career_id = to_s(user.principal_career_id);
  vm.universities = [];
  vm.careers = [];
  vm.countries = [];
  vm.submit_data = submit_data;
  vm.job_id = $stateParams.job_id;
  vm.loadedCountries = false;
  vm.loadingUniversities = false;
  vm.loadingCareers = false;
  vm.dataFilters = {};

  console.log("NewUserAcademicCtrl");

  function change_country(){
    $(document).ready(function(){
      $(document).on("change", ".country-user", function(){
        select = $(this);
        var value = select.val();
        if (vm.country_id == value){
          return;
        }
        getDataFromCountry(value);
      });
    });
  }

  FJAPI.getCountries(function(response){
    console.log("get countries");
    vm.countries = response.data;
    vm.loadedCountries = true;
    $("#country-user").select2();
    getDataFromCountry(vm.country_id);
    change_country();
    setTimeout(function(){$("#country-user").val(vm.country_id).trigger("change");}, 1000);
  }, function(err){ console.log(err); }); 

  function getDataFromCountry(country_id){
    if (vm.loadedCountries == false){ return; }
    console.log("getDataFromCountry");
    vm.loadingCareers = true;
    vm.loadingUniversities = true;

    FJAPI.getUniversitiesByCountry( country_id, function(response){
      vm.universities = response.data;
      vm.loadingUniversities = false;
      $("#university-user").select2();
      setTimeout(function(){$("#university-user").val(vm.university_id).trigger("change");}, 1000);
    }, function(err){ console.log(err); });

    FJAPI.getCareersByCountry( country_id, function(response){
      vm.careers = response.data;
      vm.loadingCareers = false;
      $("#career-user").select2({placeholder: "Agregar carreras"});
      setTimeout(function(){$("#career-user").val(vm.principal_career_id).trigger("change");}, 1000);
    }, function(err){ console.log(err); });

  }

  function submit_data(){
    vm.user.university_id = parseInt( $("#university-user").val() );
    vm.user.principal_career_id = parseInt( $("#career-user").val() );

    FJAPI.userDataAcademic( {users: vm.user}, function(response){
      localStorage.setItem("firstjob_user", JSON.stringify(response.data));
      ModalService.open({}, 'lg', 'components/users/new_user_career_status.html');
    }, function(err){ console.log(err); });
  }

});

app.controller("NewUserCareerStatusCtrl", function(FJAPI, $state, $stateParams, ModalService){
  var vm = this;
  var user = JSON.parse( localStorage.getItem("firstjob_user"));
  vm.job_id = $stateParams.job_id;
  vm.user = { looking_for: to_s(user.looking_for), career_status: to_s(user.career_status),
   years_experience: to_s(user.years_experience), age_study: to_s(user.age_study) };
   vm.submit_data = submit_data;

   function submit_data(){

    if (to_s(vm.user.career_status) != "1"){
      vm.user.age_study = null;
    }

    if (to_s(vm.user.career_status) == "3"){
      vm.user.looking_for = 3;
    }

    if (to_s(vm.user.career_status) == "1"){
      vm.user.years_experience = null;
    }

    var user = { looking_for: parseInt(vm.user.looking_for), career_status: parseInt(vm.user.career_status),
     years_experience: parseInt(vm.user.years_experience), age_study: parseInt(vm.user.age_study) };

     FJAPI.userCareerStatus( {users: user}, function(response){
      localStorage.setItem("firstjob_user", JSON.stringify(response.data));
      ModalService.open({}, 'lg', 'components/users/new_user_questions.html');

    }, function(err){console.log(err)});
   }

 });


app.controller("NewUserAbilityCtrl", function(FJAPI, $state, $stateParams, ModalService){
  var vm = this;
  var user = JSON.parse( localStorage.getItem("firstjob_user"));
  vm.user = {english_level: to_s(user.english_level), excel_level: to_s(user.excel_level)};
  vm.submit_data = submit_data;
  vm.job_id = $stateParams.job_id;
  vm.goBack = function(){
    if (user.country_id == 2){
      ModalService.open({}, 'lg', 'components/users/new_user_assistantships.html');
    }
    else
    {
      ModalService.open({}, 'lg', 'components/users/new_user_career_status.html'); 
    }
  }

  function submit_data(){
    var user = {english_level: parseInt(vm.user.english_level), excel_level: parseInt(vm.user.excel_level) };
    FJAPI.userAbility( {users: user}, function(response){
     localStorage.setItem("firstjob_user", JSON.stringify(response.data));
     if (vm.job_id){
       ModalService.open({}, 'lg', 'components/jobs/job-salary.html');
     }
     else
     {
      ModalService.dismissLastModal();
      $state.go("profile");
    }

  }, function(err){ console.log(err); });
  }
});

app.controller("NewUserSalaryCtrl", function(FJAPI, $stateParams){
  var vm = this;
  var user = JSON.parse( localStorage.getItem("firstjob_user"));
  vm.user = {minimum_salary_expected: user.minimum_salary_expected};
  vm.submit_data = submit_data;
  vm.job_id = $stateParams.job_id;

  function submit_data(){
    var user = {users: vm.user};
    FJAPI.userSalary(user, function(response){
      localStorage.setItem("firstjob_user", JSON.stringify(response.data));
    }, function(err){ console.log(err); });
  }
});

app.controller("NewUserCVCtrl", function(Upload, $timeout, ModalService, UserService){
  var vm = this;
  vm.submit_data = submit_data;
  vm.f = null;
  vm.errFile = null;

  vm.uploadFiles = function(file, errFiles){
    vm.f = file;
    vm.errFile = errFiles && errFiles[0];
  }

  function submit_data(){
    var user = UserService.getUserData();
    var headers = {'User-Email': user.email, 'User-Token': user.authentication_token }

    if (vm.f) {
      vm.f.upload = Upload.upload({
        url: baseUrl + 'user/cv-upload',
        headers: headers,
        data: {file: vm.f}
      });

      vm.f.upload.then(function (response) {
        $timeout(function () {
          vm.f.result = response.data;
          localStorage.setItem("firstjob_user", JSON.stringify(response.data));
          ModalService.dismissLastModal();
          ModalService.open({}, 'lg', 'components/jobs/job-apply-end.html');
        });
      }, function (response) {
        if (response.status > 0)
          vm.errorMsg = response.status + ': ' + response.data;
      }, function (evt) {
        vm.f.progress = Math.min(100, parseInt(100.0 * 
         evt.loaded / evt.total));
      });
    }
  }
})

app.controller("NewUserQuestionsCtrl", function(FJAPI, $state, $stateParams, ModalService){
  var vm = this;
  var user = JSON.parse( localStorage.getItem("firstjob_user"));
  vm.submit_data = submit_data;
  vm.job_id = $stateParams.job_id;
  vm.activities= {student_activities: user.student_activities, sport_activities: user.sport_activities,
   student_ind_activities: user.student_ind_activities, volunteering: user.volunteering};

   function submit_data(){
    FJAPI.userExtraActivities( {user: vm.activities}, function(response){
      localStorage.setItem("firstjob_user", JSON.stringify(response.data));
      ModalService.open({}, 'lg', 'components/users/new_user_assistantships.html');
    }, function(err){ console.log(err);});
  }

});

app.controller("NewUserAssistantshipsCtrl", function(FJAPI, $state, $stateParams, ModalService){
  var vm = this;
  var user = JSON.parse( localStorage.getItem("firstjob_user"));
  vm.user = {assistantship: user.assistantship, assistantship_number: to_s(user.assistantship_number),
   qualifications: to_s(user.qualifications)};
   vm.submit_data = submit_data;
   vm.job_id = $stateParams.job_id;

   function submit_data(){

    FJAPI.userAssistantships({users: vm.user}, function(response){
      localStorage.setItem("firstjob_user", JSON.stringify(response.data));
      ModalService.open({}, 'lg', 'components/users/new_user_ability.html');
    }, function(err){
      console.log(err);
    });
  }
})

app.controller("ProfileController", function UserController($location, FJAPI){
  var vm = this;
  vm.getProfile = getProfile;
  vm.profile = {};

  function getProfile(){
    FJAPI.getProfile(function(response){
      vm.profile = response.data;
      localStorage.setItem("firstjob_user", JSON.stringify(response.data));
    }, function(err){
      console.log(err);
    });
  }
});