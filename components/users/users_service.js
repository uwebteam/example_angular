angular.module('firstjob')
.factory("UserService", function(){

  var getPublicToken = function(){
    var storedToken = localStorage.getItem("firstjob_public_token");

    if ( storedToken == null ){
      var publicToken = Math.random().toString(36).substr(2, 12);
      localStorage.setItem("firstjob_public_token", publicToken);
    }

    return  localStorage.getItem("firstjob_public_token");
  }

  var getUserData = function(){
    return JSON.parse( localStorage.getItem("firstjob_user"));
  }

  var getResumeUrl = function(){
    return getUserData().resume;
  }

  var setShowSalaryBackButton = function(value){
    localStorage.setItem("firstjob_salary_back", value);
  }

  var getShowSalaryBackButton = function(){
    return localStorage.getItem("firstjob_salary_back");
  }

  var isUser = function(){
    return JSON.parse( localStorage.getItem("firstjob_user")) ? true : false;
  }

  return {
    getPublicToken: getPublicToken,
    getUserData: getUserData,
    getResumeUrl: getResumeUrl,
    isUser: isUser,
    getShowSalaryBackButton: getShowSalaryBackButton,
    setShowSalaryBackButton: setShowSalaryBackButton
  };

});