# FirstJob
-----
####**Requisites**

- API End-point in: **localhost:3000**

####**Install**

In order to use it, you need to have [**node.js**](https://nodejs.org/en/download/) installed.

```
#!bash

$ npm install
```


Install [**Gulp**](http://gulpjs.com/) globally.

```
#!bash

$ npm install --global gulp-cli
```


Install [**Bower**](http://bower.io/) and download dependencies.

```
#!bash

$ npm install -g bower
$ bower install
```

####**Run**

```
#!bash

$ npm start
```

####**Localhost**
If you have the API End-point running, by default the server will run on:

- Local: http://localhost:3001
- External: http://192.168.0.19:3001

-----
## **How to make a pull request to master**

- Push all your local commits to your branch.
- Update branch `master`

```
#!bash

$ git checkout master
$ git pull origin master
```
- Come back to your branch

```
#!bash

$ git checkout <your-branch-name>
```
- `fetch` and `rebase` from `master`
```
#!bash

$ git fetch origin
$ git rebase origin/master
```
- If you have conflicts, solve them.
- When everything is correct, you can push your changes.

```
#!bash

$ git push origin <your-branch-name> -f
```

- Now you're ready to make the pull request to master.