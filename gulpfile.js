/* 
  Task for assets v1.1
*/

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var autoprefixer = require('autoprefixer');
var runSequence = require('run-sequence');

// Setup
// ------------------

var paths = {
  scss: ['components/**/*.scss'],
  scssbase: ['components/base/**/*.scss'],
  base: ['components/base/*.scss'],
  css: ['components/**/*.css'],
  dist: 'assets/css/'
};

var processors = [
  autoprefixer({browsers: ['last 5 versions']})
];

// Compile Sass Files
gulp.task('compile-base', function() {
  return gulp.src(paths.base)
  .pipe($.sass({outputStyle: 'expanded'}).on('error', $.sass.logError))
  .pipe($.concat('base.css'))
  .pipe($.postcss(processors))
  .pipe(gulp.dest(paths.dist));
});


// Compile Sass Files
gulp.task('compile-scss', function() {
  return gulp.src(paths.scss)
  .pipe($.sass({outputStyle: 'expanded'}).on('error', $.sass.logError))
  .pipe($.postcss(processors))
  .pipe(gulp.dest(paths.dist));
});

// Optimization Tasks
// ------------------

// Concatenate and Optimizing CSS
gulp.task('generate-css', function() {
  return gulp.src(paths.css)
  .pipe(gulp.dest(paths.dist));
});


gulp.task('generate-min-css', function() {
  return gulp.src(paths.css)
  .pipe($.concat('styles.css'))
  .pipe(gulp.dest(paths.dist))
  .pipe($.cssnano())
  .pipe($.rename({suffix: '.min'}))
  .pipe(gulp.dest(paths.dist));
});

// Build Sequences
// ---------------

gulp.task('default', function() {
  //runSequence('compile-base', 'generate-css');
  //gulp.watch(paths.scssbase, ['compile-base']);
  gulp.watch(paths.css, ['generate-css']);
});

gulp.task('build', function() {
  //runSequence('compile-scss', 'generate-min-css');
  console.log("Info: DEPRECATED TASK.");
});
