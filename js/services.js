// GLobal Servicesssss!

angular.module('firstjob')

.factory("FJAPI", function(HTML){

  var facebookAuthEndPoint = "auth/facebook/callback.json";
  var makeRequest = HTML.makeRequest;

  var signUp = function(user, success, error){
    makeRequest(success, error, 'POST', user, "users.json");
  };

  var signIn = function(user, success, error){
    makeRequest(success, error, "POST", user, "users/sign_in.json");
  };

  var getProfile = function(success, error){
    makeRequest(success, error, 'GET', {}, "/me");
  };


  var getFavorites = function(success, error){
    makeRequest(success, error, "GET", {}, "user/favorites.json");
  }

  var getRemoved = function(success, error){
    makeRequest(success, error, "GET", {}, "user/removed.json");
  }

  var getApplications = function(success, error){
    makeRequest(success, error, "GET", {}, "user/applications.json");
  }

  var facebookAuth= function(data, success, error){
    makeRequest(success, error, "GET", {}, facebookAuthEndPoint + "?code=" + data.access_token + "&public_token=" + data.public_token);
  };

  var getCountries = function(success, error){
    makeRequest(success, error, "GET", {}, "countries.json");
  };

  var getUniversitiesByCountry = function(country_id, success, error){
    makeRequest(success, error, "GET", {}, "universities/" + country_id + ".json");
  };

  var getCareersByCountry = function(country_id, success, error){
    makeRequest(success, error, "GET", {}, "careers/" + country_id + ".json");
  };

  var newUserData = function(user, success, error){
    makeRequest(success, error, "POST", user, "users/new-user-data");
  };

  var userDataAcademic = function(user, success, error){
    makeRequest(success, error, "POST", user, "users/user-data-academic.json");
  };

  var userCareerStatus = function(user, success, error){
    makeRequest(success, error, "POST", user, "users/user-career-status.json");
  };

  var userAbility = function(user, success, error){
    makeRequest(success, error, "POST", user, "users/user-ability.json");
  };

  var userSalary = function(user, success, error){
    makeRequest(success, error, "POST", user, "users/user-salary.json");
  }

  var userAssistantships = function(user, success, error){
    makeRequest(success, error, "POST", user, "users/user-assistantships");
  }

  var clickFavorite = function(favorite, success, error){
    makeRequest(success, error, "POST", favorite , "job/favorite");
  }

  var clickRemove = function(removed, success, error){
    makeRequest(success, error, "POST", removed, "job/remove");
  }

  var getQuestions = function(looking_for, success, error){
    makeRequest(success, error, "GET", {}, "questions?type_question=" + looking_for);
  }

  var userExtraActivities = function(activities, success, error){
    makeRequest(success, error, "POST", activities, "users/user-extra-activities");
  }

  var answerJobQuestions = function(id, answers, success, error){
    makeRequest(success, error, "POST", {job_id: id, answers: answers}, "job/answer-job-questions.json");
  }

  var lostPassword = function(email, success, error){
    makeRequest(success, error, "POST", {user: email}, "users/password.json");
  }

  var getJobApplication = function(job_id, success, error){
    makeRequest(success, error, "GET", {}, "user/applications/" + job_id);
  }

  var getJobAnswers = function(job_id, success, error){
    makeRequest(success, error, "GET", {}, "user/answers/" + job_id);
  }

  var removeJobApplication = function(job_id, success, error){
    makeRequest(success, error, "DELETE", {}, "job/apply/" + job_id);
  }

  var buttonClickTracking = function(data, success, error){
    makeRequest(success, error, "POST", data, "job/button");
  }

  return {
    signUp: signUp,
    signIn: signIn,
    facebookAuth: facebookAuth,
    getProfile: getProfile,
    getFavorites: getFavorites,
    getApplications: getApplications,
    getRemoved: getRemoved,
    getCountries: getCountries,
    getUniversitiesByCountry: getUniversitiesByCountry,
    getCareersByCountry: getCareersByCountry,
    getQuestions: getQuestions,
    userExtraActivities: userExtraActivities,
    newUserData: newUserData,
    userDataAcademic: userDataAcademic,
    userCareerStatus: userCareerStatus,
    userAbility: userAbility,
    userSalary: userSalary,
    userAssistantships: userAssistantships,
    clickFavorite: clickFavorite,
    clickRemove: clickRemove,
    answerJobQuestions: answerJobQuestions,
    lostPassword: lostPassword,
    getJobApplication: getJobApplication,
    getJobAnswers: getJobAnswers,
    removeJobApplication: removeJobApplication,
    buttonClickTracking: buttonClickTracking
  };  
});