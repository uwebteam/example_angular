baseUrl = "http://localhost:3000/";

angular.module('firstjob', [
  'ui.router',
  'ui.bootstrap',
  'satellizer',
  'facebookAuthService',
  'angularUtils.directives.dirPagination',
  'ngAnimate',
  'ngFileUpload'
  ])

.run(function($rootScope, $location, UserService, ModalService, JobService, $window, $state) {
  // Initializers here ;)
  $rootScope.UserService = UserService;
  $rootScope.ModalService = ModalService;
  $rootScope.JobService = JobService;
  to_s = function(value){
    if (value == null) {
      return "";
    }
    return value.toString();
  };

  $rootScope.reloadPage = function(){
    location.reload();
  }

  $rootScope.currentURL = function(){
    var current_url = window.location.href;
    return current_url;
  }

  $rootScope.goToJobs = function(){
    ModalService.dismissLastModal();
    $state.go("jobs");
  }

  $rootScope.isSelected = function(a,b){

    if (a == null || b == null) {
      return false;
    }

    if (a.toString() == b.toString()) {
      return true;
    }
    return false;
  };

  function loadCounters(){
    FJAPI.counterJobs(function(res){
      vm.counters = res.data;
      vm.countersAll = vm.counters.all_jobs - vm.counters.fav_jobs - vm.counters.apply_jobs - vm.counters.removed_jobs ;
    }, function(err){
      console.log();
    });
  }

  // Sessions
  if (typeof(Storage) === "undefined") {
    console.log(" LocalStorage no disponible :( ");
  }

  $rootScope.$on("$locationChangeSuccess",
    function(event, current, previous, rejection) {
      $window.scrollTo(0,0);
    });
})
.config(function($authProvider) {

  //var serverUrl = "http://localhost:3000";
  var clientUrl = "http://localhost:3001";

  //$authProvider.loginUrl = serverUrl + "/users/sign_in.json";
  $authProvider.tokenName = "token";
  $authProvider.tokenPrefix = "firstjob";
});