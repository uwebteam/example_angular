angular.module('firstjob')
.factory("HTML", function($http){

  var urlApi = "http://localhost:3000/";

  var makeRequest = function(success, error, method, data, url){

    var user = JSON.parse( localStorage.getItem("firstjob_user"));

    var req = {
      method: method,
      url: urlApi + url,
      headers: {
        'Content-Type': 'application/json'
      }
    };

    if (user){
      req.headers['User-Email'] = user.email;
      req.headers['User-Token'] = user.authentication_token;
    }

    if (JSON.stringify(data) != "{}"){
      req.data = data;
    }    

    $http(req).then(function(data){
      success(data);
    }, function(err){
      error(err);
    });

    return;
  };

  return {
    makeRequest: makeRequest
  }

});