angular.module('facebookAuthService', [])
.factory('facebookService', function($q) {

    var login = function(ok, error){
        FB.getLoginStatus(function(response) {

            if (!response.authResponse){
                FB.login(function(response){

                    if (response.authResponse){
                        ok(response.authResponse);
                    }
                    else {
                        error("Usuario no acepta");
                    }
                    
                },
                {scope: 'public_profile, email'});
            }
            else {
                authResponse = FB.getAuthResponse();
                ok(authResponse);
            }
        });

    };

    var getUserData = function() {

        var deferred = $q.defer();
        FB.api('/me', {
            fields: 'last_name, email'
        }, function(response) {
            if (!response || response.error) {
                deferred.reject('Error occured');
            } else {
                deferred.resolve(response);
            }
        });

        return deferred.promise;
    };

    return {
        login: login,
        getUserData: getUserData
    }
});