// Global Controllers!

var app = angular.module('firstjob');

app.controller("SessionController", SessionController);

function SessionController($location, facebookService, FJAPI, $state, $rootScope, ModalService, UserService) {
  var vm = this;
  vm.user = JSON.parse(localStorage.getItem("firstjob_user"));
  if (localStorage.getItem("firstjob_user") && $state.current.name == "home" ){
    $state.go("jobs");
    return;
  }

  vm.email = "abel.orian@gmail.com"; //For quick test
  vm.password = "123123123."; //For quick test
  vm.currentState = $state.current.name;
  vm.facebookLogin = facebookLogin;
  vm.login = login;
  vm.signup = signup;
  vm.logout = logout;
  vm.lostPassword = lostPassword;
  vm.disableButton = false;

  function lostPassword(isModal){

    vm.disableButton = true;
    FJAPI.lostPassword({email: vm.email}, function(response){
      if (isModal){
        ModalService.open({}, 'lg', 'components/home/modals/modal-lost-password-send.html');
      }
      else
      {
        $state.go("lost-password-send");
      }

    }, function(err){
      console.log(err);
      vm.disableButton = false;
    });
  }

  function signup () {
    var user = {email: vm.email, password: vm.password, first_name: "Usuario", public_token:  UserService.getPublicToken()};
    vm.disableButton = true;

    FJAPI.signUp( user, function(response){
      localStorage.setItem("firstjob_token", response.data.authentication_token);
      localStorage.setItem("firstjob_email", response.data.email);
      localStorage.setItem("firstjob_user", JSON.stringify(response.data));
      vm.disableButton = false;
      jobRedirect(vm.currentState);
    }, function(err){
      console.log(err);
      vm.disableButton = false;
    });
  }

  function login () {
    var user = {user: {email: vm.email, password: vm.password, public_token:  UserService.getPublicToken()} };
    localStorage.removeItem("firstjob_email");
    localStorage.removeItem("firstjob_token");
    vm.disableButton = true;
    FJAPI.signIn( user, function(response){
      localStorage.setItem("firstjob_token", response.data.authentication_token);
      localStorage.setItem("firstjob_email", response.data.email);
      localStorage.setItem("firstjob_user", JSON.stringify(response.data));
      vm.disableButton = false;
      jobRedirect(vm.currentState);
    }, function(err){
      console.log(err);
      vm.disableButton = false;
    });
  }

  function facebookLogin (){
    facebookService.login(function(response){
      data = {access_token: response.accessToken, public_token: UserService.getPublicToken()};
      FJAPI.facebookAuth( data , function(response){
        localStorage.setItem("firstjob_token", response.data.authentication_token);
        localStorage.setItem("firstjob_user", JSON.stringify(response.data));
        jobRedirect(vm.currentState);
      }, function(err){
        console.log(err);
      });

    }, function(err){
      console.log(err);
    });
  }

  function jobRedirect(state){
    $rootScope.ModalService.dismissLastModal();
    if (state == 'login' || state == 'home' ){
      $state.go("jobs");
    }
    else
    {
      location.reload();
    }
  }

  function logout (){
    localStorage.removeItem("firstjob_email");
    localStorage.removeItem("firstjob_token");
    localStorage.removeItem("firstjob_user");
    //localStorage.removeItem("firstjob_public_token");
    $state.go("home");
  }
}

app.controller("StateController", function($state){
  var vm = this;
  vm.currentState = $state.current.name;
});

