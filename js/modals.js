angular.module('firstjob').controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, isModal, templateUrl, modalParams) {

  $scope.isModal = isModal;
  $scope.templateUrl = templateUrl;
  $scope.modalParams = modalParams;

  $scope.ok = function () {
    $uibModalInstance.close();
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});

angular.module('firstjob')
.factory("ModalService", function($uibModal){

  var lastModal = null;
  var oneInstance = true;
  var modalParams = {};

  var getLastModal = function(){
    return lastModal;
  }

  var dismissLastModal = function(){
    if (getLastModal()){
      lastModal.dismiss('cancel');
    }
  }

  var open = function (params, size, templateUrl) {
    if (lastModal && oneInstance){
      dismissLastModal();
    }
    modalParams = params;
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: templateUrl,
      controller: 'ModalInstanceCtrl',
      size: size,
      keyboard: false,
      backdrop: "static", //descomentar sólo cuando esté la X en la esquina de todos los modals.
      resolve: {
        isModal: function(){
          return true;
        },
        templateUrl: function(){
          return templateUrl;
        },
        modalParams: function(){
          return modalParams;
        }
      }
    });

    lastModal = modalInstance;
    modalInstance.result.then(function (selectedItem) {
    }, function () {
      //console.log('Modal dismissed at: ' + new Date());
    });
  };

  return {
    open: open,
    dismissLastModal: dismissLastModal
  };

});

angular.module('firstjob').directive('closemodal', function(ModalService){
  return {
    restrict: 'A',
    scope: true,
    link: function(scope, elem, attrs){
      elem.bind('click', function() {
        ModalService.dismissLastModal();
      });
    }
  }
});