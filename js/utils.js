function removeDots(value){
  value = value.toString();
  if (value == null){ return '';}
  return value.replace(/\./g,'');
}

function formatNumber(input)
{
  var num = removeDots(input);
  if(!isNaN(num)){
    num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
    num = num.split('').reverse().join('').replace(/^[\.]/,'');
    return num;
  }
  else {
    alert('Sólo se permiten numeros');
    return '';
  }
}

$(document).on('keyup', '#renta', function(e) {
  var input = $(this);
  var value = input.val();
  var valueNoDots = removeDots(value);
  var formatValue = formatNumber(valueNoDots);
  input.val(formatValue);
});

$(document).ready(function(){
  $(document).on("focus", "[data-type=rut]" , function(){
    $("[data-type=rut]").rut({
      formatOn: 'keyup',
      validateOn: 'change'
    });
  });
  menu();
});


function menu(){
  /* Toggle mobile menu */
  $(document).on("click", ".main-nav-toggle, .close-menu" ,function(e) {
    //e.preventDefault();
    if ($("body").width() > 992){
      return;
    }
    var obj = $('.main-nav-1');
    var sel = 'is-open';

    if ($(obj).hasClass(sel)) {
      $(obj).removeClass(sel);
      $('body').removeClass('is-behind');
    } else {
      $(obj).addClass(sel);
      $('body').addClass('is-behind');
    }
  });
}


