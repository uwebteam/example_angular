angular.module('firstjob')

.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/");

  // Use HTML5 history management: PushState
  // Drop support for IE9 - http://caniuse.com/#search=pushstate
  $locationProvider.html5Mode(true);

  // Now set up the states
  $stateProvider

  .state('home', {
    url: "/",
    templateUrl: "components/home/home.html",
    controller: "SessionController",
    controllerAs: 'login',
    cache: false
  })

  .state('jobs', {
    url: "/jobs",
    templateUrl: "components/jobs/job-cards.html",
    controller: "JobCtrl",
    controllerAs: "Job",
    cache: false
  })

  .state('company_public', {
    url: "/company/:company_id",
    templateUrl: "components/company/public.html",
    controller: "JobCtrl",
    controllerAs: "Job",
    cache: false
  })
  .state("job-favorites", {url: "/user/favorites", templateUrl: "components/jobs/job-cards.html", controller: "JobCtrl", controllerAs: "Job", cache: false})
  .state("job-applications", {url: "/user/applications", templateUrl: "components/jobs/job-cards.html", controller: "JobCtrl", controllerAs: "Job", cache: false})
  .state("job-removed", {url: "/job/removed", templateUrl: "components/jobs/job-cards.html", controller: "JobCtrl", controllerAs: "Job", cache: false})

  .state('jobs-show', {
    url: "/jobs/:job_id",
    templateUrl: "components/jobs/job-details.html",
    controller: "JobShow",
    controllerAs: "Job",
    cache: false
  })

  .state('new-job-salary', {url: "/jobs/:job_id/salary", templateUrl: "components/jobs/job-salary.html", controller: "JobShow", controllerAs: "Job"})
  .state('job-questions', {url: "/jobs/:job_id/questions", templateUrl: "components/jobs/job-questions.html", controller: "JobShow", controllerAs: "Job" })
  .state('job-apply-end', {url: "/jobs/:job_id/apply-end", templateUrl: "components/jobs/job-apply-end.html", controller: "JobShow", controllerAs: "Job" })

  .state("login", {
    url: "/login",
    templateUrl: "components/home/home.html"
  })

  .state("lost-password", {url: "/lost-password", templateUrl: "components/home/home.html", controller: "SessionController", controllerAs: "login"})
  .state("lost-password-send", {url: "/lost-password-send", templateUrl: "components/home/home.html", controller: "SessionController", controllerAs: "login"})

  .state('profile', {
    url: "/profile",
    templateUrl: "components/users/profile.html",
    controller: "ProfileController",
    controllerAs: "User",
    cache: false
  })

  .state('logout', {
  	url: "/logout",
  	controller: "LogoutController"
  })

  .state('new-user', {url: "/new-user-data/:job_id", templateUrl: "components/users/new_user_data.html", controller: "NewUserCtrl", controllerAs: "NewUser" })
  .state('new-user-academic', {url: "/new-user-academic/:job_id", templateUrl: "components/users/new_user_academic.html", controller: "NewUserAcademicCtrl", controllerAs: "NewUser" })
  .state('new-user-career-status', {url: "/new-user-career-status/:job_id", templateUrl: "components/users/new_user_career_status.html", controller: "NewUserCareerStatusCtrl", controllerAs: "NewUser"})
  .state('new-user-ability', {url: "/new-user-ability/:job_id", templateUrl: "components/users/new_user_ability.html", controller: "NewUserAbilityCtrl", controllerAs: "NewUser"})
  .state('new-user-salary', {url: "/new-user-salary/:job_id", templateUrl: "components/users/new_user_salary.html", controller: "NewUserSalaryCtrl", controllerAs: "NewUser"})
  .state('new-user-questions', {url: "/new-user-questions/:job_id", templateUrl: "components/users/new_user_questions.html", controller: "NewUserQuestionsCtrl", controllerAs: "NewUser"})
  .state('new-user-assistantships', {url: "/new-user-assistantships/:job_id", templateUrl: "components/users/new_user_assistantships.html", controller: "NewUserAssistantshipsCtrl", controllerAs: "NewUser"})

  .state('client', {
    url: "/client",
    templateUrl: "components/client/client.html"
  })
  ;

});
